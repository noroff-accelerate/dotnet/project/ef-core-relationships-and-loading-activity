# University Management System

## Overview
This .NET Core application demonstrates advanced Entity Framework Core functionalities, focusing on relationships, data loading strategies, and cascading deletes within a university system context.

## Setup
- Clone the repository.
- Ensure .NET Core and EF Core are installed.
- Run `dotnet restore` to install necessary packages.

## Features
- Demonstrates one-to-one, one-to-many, and many-to-many relationships.
- Implements and tests eager, lazy, and explicit loading strategies.
- Showcases cascading delete behavior.

## Running the Application
- Use `dotnet run` to start the application.

## Testing
- Tests are structured using the Arrange-Act-Assert (AAA) pattern.
- Includes tests for different types of relationships and data loading strategies.
- Utilizes a mocked EF Core context for isolated testing.

## Documentation
- Code is well-commented, explaining key configurations and operations.
- Tests include comments for clarity on what each test case covers.

## Challenges and Solutions
- Describe any specific challenges encountered during development and the solutions applied.

## Contributions
Contributions to enhance or extend the project are welcome. Please adhere to the existing coding and documentation standards.

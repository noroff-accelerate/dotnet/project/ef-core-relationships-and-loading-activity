﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities
{
    public class Course
    {
        public int CourseId { get; set; }
        public string Title { get; set; }
        public int? DepartmentId { get; set; }
        public Department? Department { get; set; } // For one-to-many
        public ICollection<Student> Students { get; set; } // For many-to-many
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities
{
    public class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public ICollection<Course> Courses { get; set; } // For many-to-many
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public int? InstructorId { get; set; }
        public Instructor? Head { get; set; } // For one-to-one
        public ICollection<Course> Courses { get; set; } // For one-to-many
    }
}

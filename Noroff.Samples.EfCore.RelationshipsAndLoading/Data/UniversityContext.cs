﻿using Microsoft.EntityFrameworkCore;
using Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.RelationshipsAndLoading.Data
{
    // EF Core will automatically handle the join table for the many-to-many relationship
    // between Student and Course. No explicit configuration in OnModelCreating is needed for this.
    public class UniversityContext : DbContext
    {
        public UniversityContext(DbContextOptions options) : base(options)
        {
        }

        public UniversityContext()
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public virtual DbSet<Department> Departments { get; set; } // Virtual is for lazy loading.
        public DbSet<Instructor> Instructors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // NB: Wont reconfgiure it if its already been configured (by tests for example)
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("DataSource=:memory:"); // or use SQL Server
            }
        }

        // Setup cascade deletes
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Department>()
                .HasMany(d => d.Courses)
                .WithOne(c => c.Department)
                .OnDelete(DeleteBehavior.Cascade);
        }

    }

}

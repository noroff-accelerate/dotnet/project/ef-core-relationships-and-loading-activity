﻿using Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.Relationships.Tests
{
    public class LoadingTests
    {
        // Eager Loading
        [Fact]
        public void EagerLoading_Should_Load_Departments_With_Courses()
        {
            // Arrange
            var context = TestHelpers.CreateMockedContext();
            var expectedDepartmentName = "Computer Science";
            var department = new Department { Name = expectedDepartmentName };
            department.Courses = new List<Course> { new Course { Title = "Intro to CS" }, new Course { Title = "Data Structures" } };
            context.Departments.Add(department);
            context.SaveChanges();

            // Act
            var coursesWithDepartments = TestHelpers.GetAllCoursesWithDepartments(context);
            var actualDepartmentName = coursesWithDepartments.First().Department.Name;

            // Assert
            Assert.All(coursesWithDepartments, course => Assert.NotNull(course.Department));
            Assert.Equal(expectedDepartmentName, actualDepartmentName);
        }

        // Lazy Loading
        [Fact]
        public void LazyLoading_Should_Load_Department_On_Access()
        {
            // Arrange
            var context = TestHelpers.CreateMockedContext();
            var expectedDepartmentName = "Mathematics";
            var department = new Department { Name = expectedDepartmentName };
            var course = new Course { Title = "Calculus", Department = department };
            context.Courses.Add(course);
            context.SaveChanges();

            // Act
            var retrievedCourse = TestHelpers.GetCourseByIdLazyLoad(course.CourseId, context);
            var actualDepartmentName = retrievedCourse.Department.Name;

            // Assert
            Assert.NotNull(retrievedCourse.Department);
            Assert.Equal(expectedDepartmentName, actualDepartmentName);
        }

        // Explicit Loading
        [Fact]
        public void ExplicitLoading_Should_Load_Courses_For_Department()
        {
            // Arrange
            var context = TestHelpers.CreateMockedContext();
            var expectedCourseCount = 2;
            var department = new Department { Name = "Physics" };
            department.Courses = new List<Course> { new Course { Title = "Quantum Mechanics" }, new Course { Title = "Thermodynamics" } };
            context.Departments.Add(department);
            context.SaveChanges();

            // Act
            var retrievedDepartment = TestHelpers.GetDepartmentWithCoursesExplicitly(department.DepartmentId, context);
            var actualCourseCount = retrievedDepartment.Courses.Count;

            // Assert
            Assert.Equal(expectedCourseCount, actualCourseCount);
        }

    }
}

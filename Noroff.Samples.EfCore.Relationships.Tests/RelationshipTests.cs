using Microsoft.EntityFrameworkCore;
using Noroff.Samples.EfCore.RelationshipsAndLoading.Data;
using Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities;

namespace Noroff.Samples.EfCore.Relationships.Tests
{
    public class RelationshipTests
    {
        // One-to-One
        [Fact]
        public void Department_Should_Have_Associated_Instructor_As_Head()
        {
            // Arrange
            var expectedInstructorName = "John Smith";
            using var context = TestHelpers.CreateMockedContext();
            var instructor = new Instructor { Name = expectedInstructorName };
            var department = new Department { Name = "Physics", Head = instructor };

            // Act
            context.Departments.Add(department);
            context.SaveChanges();
            var actualInstructorName = context.Departments.Include(d => d.Head).FirstOrDefault().Head.Name;

            // Assert
            Assert.Equal(expectedInstructorName, actualInstructorName);
        }

        // One-to-Many
        [Fact]
        public void Department_Should_Have_Multiple_Courses()
        {
            // Arrange
            var expectedCourseCount = 2;
            using var context = TestHelpers.CreateMockedContext();
            var department = new Department { Name = "Mathematics", Courses = new List<Course> { new Course { Title = "Algebra" }, new Course { Title = "Calculus"} } };

            // Act
            context.Departments.Add(department);
            context.SaveChanges();

            var actualCourseCount = context.Departments.Include(d => d.Courses).FirstOrDefault().Courses.Count;

            // Assert
            Assert.Equal(expectedCourseCount, actualCourseCount);
        }

        // Many-to-Many
        [Fact]
        public void Student_Should_Be_Enrolled_In_Multiple_Courses()
        {
            // Arrange
            var expectedEnrolledCourseCount = 2;
            using var context = TestHelpers.CreateMockedContext();
            var student = new Student { Name = "Alice", Courses = new List<Course> { new Course { Title = "Biology" }, new Course { Title = "Chemistry" } } };

            // Act
            context.Students.Add(student);
            context.SaveChanges();
            var actualEnrolledCourseCount = context.Students.Include(s => s.Courses).FirstOrDefault().Courses.Count;

            // Assert
            Assert.Equal(expectedEnrolledCourseCount, actualEnrolledCourseCount);
        }

        

    }
}
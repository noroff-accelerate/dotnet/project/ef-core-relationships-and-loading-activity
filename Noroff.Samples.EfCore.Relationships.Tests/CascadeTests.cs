﻿using Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.Relationships.Tests
{
    public class CascadeTests
    {
        [Fact]
        public void Deleting_Department_Should_Cascade_Delete_Related_Courses()
        {
            // Arrange
            var context = TestHelpers.CreateMockedContext();
            var department = new Department { Name = "Engineering" };
            department.Courses = new List<Course> { new Course { Title = "Engineering 101" }, new Course { Title = "Engineering 102" } };
            context.Departments.Add(department);
            context.SaveChanges();

            // Act
            context.Departments.Remove(department);
            context.SaveChanges();

            var coursesCount = context.Courses.Count();

            // Assert
            Assert.Equal(0, coursesCount);
        }

    }
}

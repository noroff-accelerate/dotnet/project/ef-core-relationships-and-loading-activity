﻿using Microsoft.EntityFrameworkCore;
using Noroff.Samples.EfCore.RelationshipsAndLoading.Data;
using Noroff.Samples.EfCore.RelationshipsAndLoading.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noroff.Samples.EfCore.Relationships.Tests
{
    internal static class TestHelpers
    {

        public static UniversityContext CreateMockedContext()
        {
            var options = new DbContextOptionsBuilder<UniversityContext>()
                .UseSqlite($"DataSource=file:{Guid.NewGuid()};mode=memory;cache=shared")
                .Options;

            var context = new UniversityContext(options);
            context.Database.OpenConnection();
            context.Database.EnsureCreated();
            return context;
        }

        internal static List<Course> GetAllCoursesWithDepartments(UniversityContext context)
        {
            return context.Courses.Include(c => c.Department).ToList();
        }

        // Assuming virtual navigation properties are set up in the models
        internal static Course GetCourseByIdLazyLoad(int courseId, UniversityContext context)
        {
            return context.Courses.Single(c => c.CourseId == courseId); // Department is loaded when accessed
        }


        internal static Department GetDepartmentWithCoursesExplicitly(int departmentId, UniversityContext context)
        {
            var department = context.Departments.Single(d => d.DepartmentId == departmentId);
            context.Entry(department).Collection(d => d.Courses).Load(); // Explicitly load courses
            return department;
        }

    }
}
